import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_receiver_page/utils.dart';
import 'package:flutter_webrtc_sdk_example/page/webrtc_receiver_page/webrtc_receiver_bloc.dart';
import 'package:flutter_webrtc_sdk_example/widgets/buttons.dart';

class WebRTCReceiverPage extends StatefulWidget {
  final Uri? url;

  const WebRTCReceiverPage({Key? key, this.url}) : super(key: key);

  @override
  State<WebRTCReceiverPage> createState() => _WebRTCReceiverPageState();
}

class _WebRTCReceiverPageState extends State<WebRTCReceiverPage> {
  @override
  void initState() {
    context.read<PlaylistCubit>().setUrl(widget.url);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(children: [
      Center(child: Player(context.read<PlaylistCubit>().controller)),
      HideOnIdle(
          child: SizedBox.expand(
              child: Align(alignment: Alignment.bottomCenter, child: _controls(context))))
    ]));
  }

  Widget _controls(BuildContext context) {
    final state = context.watch<PlaylistCubit>().state;

    String? init = "webrtcs://api.fastocloud.com/64b8d314266162b331690246";
    if (state.url != null) {
      init = state.url.toString();
    }
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(children: [
          const SizedBox(width: 8),
          Expanded(
              child: TextFieldEx(
                  key: ValueKey<Uri?>(state.url),
                  init: init,
                  readOnly: state is PlayerLoadingState,
                  hintText: 'Url',
                  onFieldSubmit: (String data) {
                    context.read<PlaylistCubit>().setUrl(tryParseRaw(data));
                  })),
          MuteButton.icon(context.read<PlaylistCubit>().controller,
              mutedIcon: Icons.volume_off_rounded, unmutedIcon: Icons.volume_up_rounded),
          const FullscreenButton(),
          IconButton(
              icon: const Icon(Icons.paste),
              onPressed: state is PlayerLoadingState
                  ? null
                  : () {
                      FlutterClipboard.paste().then((String data) {
                        context.read<PlaylistCubit>().setUrl(tryParseRaw(data));
                      });
                    }),
          IconButton(
              icon: const Icon(Icons.clear),
              onPressed: () {
                context.read<PlaylistCubit>().setUrl(null);
              })
        ]));
  }
}
