part of signaling;

enum WHEPType { Client, Server }

class SignalingPlaylistWhep extends SignalingStart {
  WHEPType _type = WHEPType.Client;
  Uri? _url;

  bool _waitingForCandidates = false;
  Timer? _iceGatheringTimeout;
  Session? _session;
  String? _resource;

  SignalingPlaylistWhep([Map<String, dynamic>? iceServers]) : super(iceServers);

  final List<MediaStream> _remoteStreams = <MediaStream>[];

  StreamStateCallback? onAddRemoteStream;
  StreamStateCallback? onRemoveRemoteStream;

  final Map<String, dynamic> _dcConstraints = {'mandatory': {}, 'optional': []};

  @override
  void onSdpSemantics(Session session, RTCPeerConnection pc) {
    switch (sdpSemantics) {
      case 'plan-b':
        pc.onAddStream = (MediaStream stream) {
          onAddRemoteStream?.call(session, stream);
          _remoteStreams.add(stream);
        };
        pc.onRemoveStream = (stream) {
          onRemoveRemoteStream?.call(session, stream);
          _remoteStreams.removeWhere((it) {
            return (it.id == stream.id);
          });
        };
        break;
      case 'unified-plan':
        pc.onTrack = (event) {
          developer.log('onTrack: ${event.track.kind}');
          onAddRemoteStream?.call(session, event.streams[0]);
        };
        pc.onRemoveTrack = (MediaStream stream, MediaStreamTrack track) {
          developer.log('onRemoveTrack: $stream; $track');
          onRemoveRemoteStream?.call(session, stream); // ?
        };
        break;
    }
  }

  String? _getResourceUrlFromHeaders(Uri url, Map<String, String> headers) {
    final String? location = headers['location'];
    if (location != null && location.startsWith('/')) {
      final Uri resourceUrl = Uri.parse(url.origin + location);
      return resourceUrl.toString();
    }
    return location;
  }

  Future<void> connect(String url, bool audio, bool video) async {
    onSignalingStateChange?.call(SignalingState.connectionOpen);

    final session = Session(peerId: _selfId);
    session.pc = await _initSession(session, _dcConstraints);
    _resource = null;
    _session = session;
    _url = Uri.parse(url);

    onCallStateChange?.call(_session, CallState.callStateNew);

    final pc = _session!.pc;
    _iceGatheringTimeout?.cancel();

    if (_type == WHEPType.Client && pc != null) {
      if (video) {
        pc.addTransceiver(
            kind: RTCRtpMediaType.RTCRtpMediaTypeVideo,
            init: RTCRtpTransceiverInit(direction: TransceiverDirection.RecvOnly));
      }
      if (audio) {
        pc.addTransceiver(
            kind: RTCRtpMediaType.RTCRtpMediaTypeAudio,
            init: RTCRtpTransceiverInit(direction: TransceiverDirection.RecvOnly));
      }

      final offer = await pc.createOffer();
      // To add NACK in offer we have to add it manually see https://bugs.chromium.org/p/webrtc/issues/detail?id=4543 for details
      if (offer.sdp != null) {
        final regexp = RegExp(r'a=rtpmap:(\d+) opus/48000/2');

        final match = regexp.firstMatch(offer.sdp!);
        if (match != null) {
          final val = match.group(1);
          offer.sdp = offer.sdp!
              .replaceAll('opus/48000/2\r\n', 'opus/48000/2\r\na=rtcp-fb:' + val! + ' nack\r\n');
        }
      }

      try {
        pc.setLocalDescription(offer);
        _waitingForCandidates = true;
        _iceGatheringTimeout = Timer(const Duration(milliseconds: 2000), _onIceGatheringTimeout);
      } catch (error) {
        _onError(error);
      }
    } else if (pc != null) {
      final response = await http.post(_url!, headers: {'Content-Type': 'application/sdp'});

      if (response.statusCode == 201) {
        _resource = _getResourceUrlFromHeaders(_url!, response.headers);
        final body = response.body;
        await pc.setRemoteDescription(RTCSessionDescription(body, 'offer'));
        final answer = await pc.createAnswer();

        try {
          pc.setLocalDescription(answer);
          _waitingForCandidates = true;
          _iceGatheringTimeout = Timer(const Duration(milliseconds: 2000), _onIceGatheringTimeout);
        } catch (error) {
          _onError(error);
        }
      }
    }
  }

  void _onIceGatheringTimeout() {
    if (!_waitingForCandidates) {
      return;
    }

    _onDoneWaitingForCandidates();
  }

  Future<void> _onDoneWaitingForCandidates() async {
    _waitingForCandidates = false;
    _iceGatheringTimeout?.cancel();
    if (_type == WHEPType.Client) {
      await _sendOffer();
    } else {
      await _sendAnswer();
    }
  }

  Future<void> _sendOffer() async {
    if (_session == null) {
      return;
    }

    final pc = _session!.pc!;
    final offer = await pc.getLocalDescription();
    if (_type == WHEPType.Client && offer != null) {
      final body = offer.sdp!;
      final resp = http.post(_url!, headers: {'Content-Type': 'application/sdp'}, body: body);
      resp.then((response) {
        if (response.statusCode == 201) {
          _resource = _getResourceUrlFromHeaders(_url!, response.headers);
          final body = response.body;
          pc.setRemoteDescription(RTCSessionDescription(body, 'answer'));
        } else if (response.statusCode == 400) {
          _type = WHEPType.Server;
          onSignalingStateChange?.call(SignalingState.connectionNeedReconnect);
        }
      }, onError: (error) {});
    }
  }

  Future<void> _sendAnswer() async {
    if (_session == null) {
      return;
    }

    if (_resource != null) {
      final answer = await _session!.pc!.getLocalDescription();
      final body = answer!.sdp;
      final response = await http.patch(Uri.parse(_resource!),
          headers: {'Content-Type': 'application/sdp'}, body: body);

      if (response.statusCode != 201) {
        _onError('sendAnswer response: ${response.statusCode}');
      }
    }
  }

  @override
  void close() {
    _session?.close();
    onSignalingStateChange?.call(SignalingState.connectionClosed);
  }

  void _onError(dynamic error) {
    developer.log('Error: $error');
    onSignalingStateChange?.call(SignalingState.connectionError);
  }

  void _onIceGatheringStateChange(RTCIceGatheringState state) {
    if (state != RTCIceGatheringState.RTCIceGatheringStateComplete || !_waitingForCandidates) {
      return;
    }

    _onDoneWaitingForCandidates();
  }

  Future<RTCPeerConnection> _initSession(Session session, Map<String, dynamic> config) async {
    final args = <String, dynamic>{...iceServers, 'sdpSemantics': sdpSemantics};
    developer.log('initSession: $args');
    final RTCPeerConnection pc = await createPeerConnection(args, config);

    onSdpSemantics(session, pc);

    pc.onIceCandidate = (RTCIceCandidate candidate) {};
    pc.onIceGatheringState = _onIceGatheringStateChange;
    pc.onIceConnectionState = (state) {
      developer.log('onIceConnectionState: $state');
      if (state == RTCIceConnectionState.RTCIceConnectionStateConnected) {
        onCallStateChange?.call(session, CallState.callStateConnected);
      }
    };

    return pc;
  }
}
