import 'package:flutter/material.dart';
import 'package:flutter_webrtc_sdk/src/controller/webrtc_player_bloc.dart';
import 'package:flutter_webrtc_sdk/src/widgets/custom_webrtc_player.dart';

class WebRTCPlayer extends StatelessWidget {
  final WebRTCPlayerBloc controller;
  final double aspectRatio;
  final Widget init;
  final Widget? child;

  const WebRTCPlayer(
      {Key? key,
      required this.controller,
      this.aspectRatio = 16 / 9,
      this.init = const SizedBox(),
      this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomWebRTCPlayer(
        controller: controller,
        playerBuilder: (_, __, player) {
          return Stack(
              alignment: Alignment.center,
              children: [SizedBox.expand(child: player), if (child != null) child!]);
        },
        aspectRatio: aspectRatio,
        init: init);
  }
}
