import 'package:flutter/material.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart';

class Player extends StatelessWidget {
  final WebRTCPlayerBlocBase controller;
  final double aspectRatio;

  const Player(this.controller, {this.aspectRatio = 16 / 9});

  @override
  Widget build(BuildContext context) {
    return CustomWebRTCPlayer(
        controller: controller,
        aspectRatio: aspectRatio,
        playerBuilder: (_, __, player) {
          return Stack(alignment: Alignment.center, children: [
            SizedBox.expand(child: player),
            if (!controller.hasVideo) const Icon(Icons.videocam_off_rounded)
          ]);
        },
        init: Container(
            color: Colors.black,
            child: const Center(child: Icon(Icons.warning, color: Colors.white))));
  }
}
