library signaling;

import 'dart:async';
import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/foundation.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:http/http.dart' as http;
import 'package:random_string/random_string.dart';

import '../utils/device_info.dart' if (dart.library.js) '../utils/device_info_web.dart';
import '../websocket/websocket.dart';
import 'session.dart';

part 'signaling_call.dart';
part 'signaling_playlist.dart';
part 'signaling_playlist_whep.dart';

const String kFrom = 'from';
const String kTo = 'to';
const String kSessionID = 'session_id';

// comands
const String kNewCommand = 'new';
const String kOfferCommand = 'offer';
const String kAnswerCommand = 'answer';
const String kCandidateCommand = 'candidate';
const String kByeCommand = 'bye';

enum SignalingState { connectionOpen, connectionClosed, connectionError, connectionNeedReconnect }

enum CallState { callStateNew, callStateRinging, callStateInvite, callStateConnected, callStateBye }

/*
 * callbacks for Signaling API.
 */
typedef SignalingStateCallback = void Function(SignalingState state);
typedef CallStateCallback = void Function(Session? session, CallState state);
typedef StreamStateCallback = void Function(Session session, MediaStream stream);

abstract class SignalingStart {
  static const _iceServers = <String, dynamic>{
    'iceServers': [
      {'urls': 'stun:stun.fastocloud.com:3478'},
      {
        'urls': 'turn:turn.fastocloud.com:5349',
        'credential': 'fastocloud',
        'username': 'fastocloud'
      },
    ]
  };

  final String _selfId = randomNumeric(8);
  String get sdpSemantics => WebRTC.platformIsWindows ? 'plan-b' : 'unified-plan';
  final Map<String, dynamic> iceServers;

  SignalingStateCallback? onSignalingStateChange;
  CallStateCallback? onCallStateChange;

  SignalingStart([Map<String, dynamic>? iceServers]) : iceServers = iceServers ?? _iceServers;

  @protected
  void onSdpSemantics(Session session, RTCPeerConnection pc);

  void bye() {}

  void close() {}
}

abstract class SignalingBase extends SignalingStart {
  final String streamId;
  late final WebSocketDelegate _socket;
  final Map<String, Session> _sessions = {};

  SignalingBase(this.streamId, [Map<String, dynamic>? iceServers]) : super(iceServers);

  @protected
  void onMessage(dynamic message);

  Future<void> connect(String url) async {
    _socket = WebSocketDelegate(url, onMessage: _onMessage, onError: _onError, onClose: _onClose);

    await _socket.connect();

    onSignalingStateChange?.call(SignalingState.connectionOpen);

    _send(kNewCommand, {
      kFrom: _selfId,
      kTo: streamId,
      'name': DeviceInfo.label,
      'user_agent': DeviceInfo.userAgent,
    });
  }

  void _onMessage(dynamic message) {
    final decoded = jsonDecode(message);
    developer.log('[RECEIVE]: $message');
    onMessage(decoded);
  }

  void _onError(dynamic error) {
    developer.log('Error: $error');
    onSignalingStateChange?.call(SignalingState.connectionError);
  }

  void _onClose(int? code, String? reason) {
    developer.log('Closed by server [$code => $reason]!');
    onSignalingStateChange?.call(SignalingState.connectionClosed);
  }

  @override
  void bye() {
    _sessions[streamId]?.close();
    _send(kByeCommand, {kFrom: _selfId, kTo: streamId});
    super.bye();
  }

  @override
  void close() {
    for (final session in _sessions.values) {
      session.close();
    }

    _sessions.clear();
    _socket.close();
    super.close();
  }

  void _send(event, data) {
    final request = {'type': event, 'data': data};

    _socket.send(jsonEncode(request));
    developer.log('[SEND]: $event: $data');
  }

  Future<RTCPeerConnection> _initSession(Session session, Map<String, dynamic> config) async {
    final args = <String, dynamic>{...iceServers, 'sdpSemantics': sdpSemantics};
    developer.log('initSession: $args');
    final RTCPeerConnection pc = await createPeerConnection(args, config);

    onSdpSemantics(session, pc);

    pc.onIceCandidate = (RTCIceCandidate candidate) {
      _send(kCandidateCommand, {
        kFrom: _selfId,
        kTo: streamId,
        'sdpMLineIndex': candidate.sdpMLineIndex,
        'sdpMid': candidate.sdpMid,
        'candidate': candidate.candidate
      });
    };

    pc.onIceConnectionState = (state) {
      developer.log('onIceConnectionState: $state');
      if (state == RTCIceConnectionState.RTCIceConnectionStateConnected) {
        onCallStateChange?.call(session, CallState.callStateConnected);
      }
    };

    return pc;
  }
}
