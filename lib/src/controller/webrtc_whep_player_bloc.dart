part of 'webrtc_player_bloc.dart';

class WhepPlayerController extends WebRTCPlayerBloc<SignalingPlaylistWhep> {
  @override
  Future<void> connect(String url, [Map<String, dynamic>? iceServers]) {
    if (_signaling != null) {
      _signaling!.close();
    }
    _signaling = SignalingPlaylistWhep(iceServers)
      ..onSignalingStateChange = (SignalingState state) {
        switch (state) {
          case SignalingState.connectionNeedReconnect:
            _signaling!.close();
            _emit(ConnectingIPlayerState());
            _signaling!.connect(url, true, true);
            break;
          case SignalingState.connectionClosed:
          case SignalingState.connectionError:
          case SignalingState.connectionOpen:
            break;
        }
      }
      ..onCallStateChange = (Session? session, CallState state) {
        switch (state) {
          case CallState.callStateNew:
            _emit(LoadingIPlayerState());
            break;
          case CallState.callStateBye:
            renderer.srcObject = null;
            _emit(LoadingIPlayerState());
            onChangedStream?.call(null);
            break;
          case CallState.callStateConnected:
            _emit(PlayingIPlayerState(url));
            break;
          case CallState.callStateInvite:
          case CallState.callStateRinging:
            // TODO: ignored
            break;
        }
      }
      ..onAddRemoteStream = ((_, stream) {
        renderer.srcObject = stream;
        _emit(ReadyIPlayerState(url));
        onChangedStream?.call(stream);
      })
      ..onRemoveRemoteStream = ((_, stream) {
        renderer.srcObject = null;
        _emit(LoadingIPlayerState());
        onChangedStream?.call(null);
      });

    _emit(ConnectingIPlayerState());
    return _signaling!.connect(url, true, true);
  }
}
