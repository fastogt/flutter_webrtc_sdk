import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart';

class PlayerIdState {
  final Uri? url;

  const PlayerIdState(this.url);
}

class PlayerInitState extends PlayerIdState {
  const PlayerInitState() : super(null);
}

class PlayerLoadingState extends PlayerIdState {
  const PlayerLoadingState(Uri url) : super(url);
}

class WhepPlaylistCubit extends Cubit<PlayerIdState> {
  final controller = WhepPlayerController();

  WhepPlaylistCubit() : super(const PlayerInitState());

  void setUrl(Uri? url) async {
    if (url == null) {
      emit(const PlayerInitState());
      controller.bye();
      return;
    }

    emit(PlayerLoadingState(url));

    controller.connect(url.toString());

    await controller.stream.firstWhere((e) => e is PlayingIPlayerState);
    emit(PlayerIdState(url));
  }

  @override
  Future<void> close() async {
    controller.dispose();
    super.close();
  }
}
