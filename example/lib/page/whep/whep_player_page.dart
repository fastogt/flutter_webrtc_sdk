import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_sdk/flutter_webrtc_sdk.dart';
import 'package:flutter_webrtc_sdk_example/page/whep/whep_bloc.dart';

class WHEPplayerPage extends StatefulWidget {
  const WHEPplayerPage({super.key});

  @override
  _WHEPplayerPageState createState() => _WHEPplayerPageState();
}

class _WHEPplayerPageState extends State<WHEPplayerPage> {
  final TextEditingController _urlController = TextEditingController.fromValue(
      const TextEditingValue(text: 'https://whep5.fastocloud.com/api/v2/whep/channel/test332'));

  void _connect() {
    context.read<WhepPlaylistCubit>().setUrl(Uri.parse(_urlController.text));
  }

  void _disconnect() async {
    context.read<WhepPlaylistCubit>().setUrl(null);
  }

  @override
  void dispose() {
    _urlController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.sizeOf(context);
    final controller = context.read<WhepPlaylistCubit>().controller;
    Widget? _drawChild() {
      try {
        if (!controller.hasVideo) {
          return const Icon(Icons.videocam_off_rounded);
        }
        return null;
      } catch (error) {
        return null;
      }
    }

    return Scaffold(
        body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 60, vertical: 20),
            child: Column(children: [
              TextField(
                  controller: _urlController,
                  decoration: const InputDecoration(labelText: 'Enter WHEP Server URL')),
              const SizedBox(height: 20),
              Container(
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.white.withOpacity(0.4))),
                  height: size.height / 1.5,
                  width: size.width,
                  child: WebRTCPlayer(
                      controller: controller,
                      child: _drawChild(),
                      init: Container(
                          color: Colors.black,
                          child: const Center(child: Icon(Icons.warning, color: Colors.white))))),
              const SizedBox(height: 40),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                CustomWhepButton(onClick: _connect, text: 'Connect', color: Colors.white70),
                CustomWhepButton(onClick: _disconnect, text: 'Disconnect', color: Colors.red)
              ])
            ])));
  }
}

enum ButtonStateEnum { loading, loaded }

class CustomWhepButton extends StatefulWidget {
  final String text;
  final VoidCallback onClick;
  final Color color;

  const CustomWhepButton(
      {required this.text, required this.onClick, required this.color, super.key});

  @override
  State<CustomWhepButton> createState() => _CustomWhepButtonState();
}

class _CustomWhepButtonState extends State<CustomWhepButton> {
  ButtonStateEnum state = ButtonStateEnum.loaded;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          state = ButtonStateEnum.loading;
          widget.onClick();
          state = ButtonStateEnum.loaded;
        });
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(8)), color: widget.color),
        child:
            state == ButtonStateEnum.loaded ? Text(widget.text) : const CircularProgressIndicator(),
      ),
    );
  }
}
