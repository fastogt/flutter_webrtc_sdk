part of 'webrtc_player_bloc.dart';

class PlaylistRTCPlayerController extends WebRTCWebSockerPlayerBloc<SignalingPlaylist> {
  @override
  Future<void> connectImpl(String url, String sid, [Map<String, dynamic>? iceServers]) async {
    final info = ConnectionInfo(url.toString());
    final wshost = info.socketUrl;

    _id = sid;
    if (_signaling != null) {
      _signaling!.close();
    }
    _signaling = SignalingPlaylist(sid, iceServers)
      ..onSignalingStateChange = (SignalingState state) {
        switch (state) {
          case SignalingState.connectionNeedReconnect:
          case SignalingState.connectionClosed:
          case SignalingState.connectionError:
          case SignalingState.connectionOpen:
            break;
        }
      }
      ..onCallStateChange = (Session? session, CallState state) {
        switch (state) {
          case CallState.callStateNew:
            _emit(LoadingIPlayerState());
            break;
          case CallState.callStateBye:
            renderer.srcObject = null;
            _emit(LoadingIPlayerState());
            onChangedStream?.call(null);
            break;
          case CallState.callStateConnected:
            _emit(PlayingIPlayerState(url));
            break;
          case CallState.callStateInvite:
          case CallState.callStateRinging:
            // TODO: ignored
            break;
        }
      }
      ..onAddRemoteStream = ((_, stream) {
        renderer.srcObject = stream;
        _emit(ReadyIPlayerState(url));
        onChangedStream?.call(stream);
      })
      ..onRemoveRemoteStream = ((_, stream) {
        renderer.srcObject = null;
        _emit(LoadingIPlayerState());
        onChangedStream?.call(null);
      });

    _emit(ConnectingIPlayerState());
    return _signaling!.connect('$wshost/webrtc_out');
  }
}
