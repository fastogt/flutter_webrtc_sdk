import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:flutter_webrtc_sdk/src/signaling/session.dart';
import 'package:flutter_webrtc_sdk/src/signaling/signaling_base.dart';
import 'package:rxdart/subjects.dart';

part 'webrtc_call_bloc.dart';

part 'webrtc_playlist_bloc.dart';

part 'webrtc_whep_player_bloc.dart';

abstract class IPlayerState {}

class InitIPlayerState extends IPlayerState {}

class ConnectingIPlayerState extends IPlayerState {}

class LoadingIPlayerState extends IPlayerState {}

class ErrorIPlayerState extends IPlayerState {
  final dynamic error;
  ErrorIPlayerState(this.error);
}

class ReadyIPlayerState extends IPlayerState {
  final String url;
  ReadyIPlayerState(this.url);
}

class PlayingIPlayerState extends IPlayerState {
  final String url;
  PlayingIPlayerState(this.url);
}

typedef MediaStreamChangedCallback = void Function(MediaStream? stream);

abstract class WebRTCPlayerBlocBase<S extends SignalingStart> {
  final _controller = BehaviorSubject<IPlayerState>();
  final renderer = RTCVideoRenderer();

  MediaStreamChangedCallback? onChangedStream;
  S? _signaling;

  Stream<IPlayerState> get stream => _controller.stream;

  MediaStream? get mediaStream => renderer.srcObject;

  WebRTCPlayerBlocBase() {
    _emit(InitIPlayerState());
    renderer.initialize();
  }

  bool get hasAudio {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    return mediaStream!.getAudioTracks().isNotEmpty;
  }

  bool get hasVideo {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    return mediaStream!.getVideoTracks().isNotEmpty;
  }

  bool get muted {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    final tracks = mediaStream!.getAudioTracks();
    if (tracks.isNotEmpty) {
      return !tracks[0].enabled;
    }
    throw 'No audio tracks';
  }

  void toggleMute() {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    final tracks = mediaStream!.getAudioTracks();
    if (tracks.isNotEmpty) {
      final enabled = tracks[0].enabled;
      tracks[0].enabled = !enabled;
    } else {
      throw 'No audio tracks';
    }
  }

  void setMute(bool value) {
    if (mediaStream == null) {
      throw 'Source is not initialized';
    }
    final tracks = mediaStream!.getAudioTracks();
    if (tracks.isNotEmpty) {
      tracks[0].enabled = value;
    } else {
      throw 'No audio tracks';
    }
  }

  void bye() {
    _signaling?.bye();
    _emit(InitIPlayerState());
  }

  void dispose() {
    bye();
    renderer.dispose();
    _controller.close();
    _signaling?.close();
  }

  void _emit(IPlayerState state) {
    if (!_controller.isClosed) {
      _controller.add(state);
    }
  }
}

abstract class WebRTCPlayerBloc<S extends SignalingStart> extends WebRTCPlayerBlocBase<S> {
  Future<void> connect(String url, [Map<String, dynamic>? iceServers]);
}

class ConnectionInfo {
  final String originalUrl;

  ConnectionInfo(this.originalUrl);

  String get socketUrl => _generateUrlWithProtocol('ws', 'wss');

  String get restApiUrl => _generateUrlWithProtocol('http', 'https');

  String _generateUrlWithProtocol(String unsecure, String secure) {
    final uri = Uri.parse(originalUrl);
    var host = uri.host;

    if (uri.hasPort) {
      host += ':${uri.port}';
    }

    final useSecureProtocols =
        uri.isScheme('https') || uri.isScheme('wss') || uri.isScheme('webrtcs');

    return '${useSecureProtocols ? secure : unsecure}://$host';
  }
}

abstract class WebRTCWebSockerPlayerBloc<S extends SignalingBase> extends WebRTCPlayerBlocBase<S> {
  String? _id;

  String? get id => _id;

  Future<void> connect(String url, String sid, [Map<String, dynamic>? iceServers]) {
    _id = sid;
    return connectImpl(url, _id!, iceServers);
  }

  Future<void> connectImpl(String url, String sid, [Map<String, dynamic>? iceServers]);

  @override
  void bye() {
    _signaling?.bye();
    _emit(InitIPlayerState());
  }
}
