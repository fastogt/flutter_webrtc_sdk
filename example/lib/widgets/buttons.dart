import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class FullscreenButton extends StatelessWidget {
  const FullscreenButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () => FullscreenManager.instance.toggleFullsreen(),
      icon: const Icon(Icons.fullscreen),
    );
  }
}
