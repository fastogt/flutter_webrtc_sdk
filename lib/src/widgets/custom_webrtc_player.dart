import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:flutter_webrtc_sdk/src/controller/webrtc_player_bloc.dart';

typedef PlayerBuilder = Widget Function(
    BuildContext context, WebRTCPlayerBlocBase controller, Widget player);

class CustomWebRTCPlayer extends StatelessWidget {
  static const _initFallback = Center(child: CircularProgressIndicator(color: Colors.green));
  static const _connetingFallback =
      Center(child: CircularProgressIndicator(color: Colors.lightGreen));
  static const _loadingFallback = Center(child: CircularProgressIndicator(color: Colors.yellow));
  static const _readyFallback = Center(child: CircularProgressIndicator(color: Colors.orange));
  static const _errorFallback = Center(child: CircularProgressIndicator(color: Colors.red));

  final WebRTCPlayerBlocBase controller;
  final PlayerBuilder playerBuilder;
  final double aspectRatio;
  final Widget init, connecting, loading, ready, error;

  const CustomWebRTCPlayer(
      {Key? key,
      required this.controller,
      required this.playerBuilder,
      required this.aspectRatio,
      this.init = _initFallback,
      this.connecting = _connetingFallback,
      this.loading = _loadingFallback,
      this.ready = _readyFallback,
      this.error = _errorFallback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
        aspectRatio: aspectRatio,
        child: Container(
            color: Colors.black,
            child: StreamBuilder<IPlayerState>(
                stream: controller.stream,
                builder: (ctx, snapshot) {
                  return _player(ctx, snapshot.data);
                })));
  }

  Widget _player(BuildContext context, IPlayerState? state) {
    if (state is PlayingIPlayerState) {
      final player = RTCVideoView(controller.renderer);
      return playerBuilder(context, controller, player);
    } else if (state is ReadyIPlayerState) {
      return ready;
    } else if (state is LoadingIPlayerState) {
      return loading;
    } else if (state is ConnectingIPlayerState) {
      return connecting;
    } else if (state is ErrorIPlayerState) {
      return error;
    }

    return init;
  }
}
