import 'dart:convert';

Uri? tryParseRaw(String uri) {
  try {
    return Uri.tryParse(uri);
  } on FormatException {
    return null;
  }
}

Uri? tryParseConnect(String uri) {
  try {
    final decoded = base64Decode(uri);
    final parsed = utf8.decode(decoded);
    return tryParseRaw(parsed);
  } on FormatException {
    return null;
  }
}
