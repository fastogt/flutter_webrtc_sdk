import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc_sdk_example/page/whep/whep_bloc.dart';

import 'package:flutter_webrtc_sdk_example/page/whep/whep_player_page.dart';

void main() {
  runApp(BlocProvider(
      create: (_) {
        return WhepPlaylistCubit();
      },
      child: MaterialApp(theme: ThemeData.dark(), home: const WHEPplayerPage())));
}
